package no.noroff.footballapi;

import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class EndpointTest extends AbstractTest {

    @Test
    public void getProbabilities() throws Exception {
        String uri = "localhost:8080/";
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)
                .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        String content = mvcResult.getResponse().getContentAsString();
        FootballInformation[] productlist = super.mapFromJson(content, FootballInformation[].class);
        assertTrue(productlist.length > 0);
    }
}
