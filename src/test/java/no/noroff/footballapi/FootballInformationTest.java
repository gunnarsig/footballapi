package no.noroff.footballapi;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class FootballInformationTest {
    FootballInformation fi;
    @BeforeEach
    void setUp () {
        fi = new FootballInformation();
    }

    @Test
    void testCanRetriveClassic1x2Probability() {
        fi.setClassic1X2Probability(0.434);
        assertEquals(0.434, fi.getClassic1X2Probability());
    }

    @Test
    void getClassic1x2Probability() {
    }

    @Test
    void testIsExpired() {
        fi.setIsExpired(true);
        assertEquals(true, fi.getIsExpired());
    }



}