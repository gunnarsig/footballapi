package no.noroff.footballapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FootballapiApplication {

	/*

	We used the ping-pong technique, alternating making tests and getting them to work.
	In retrospect we could have made the tests in smaller increments to change more often,
	but it was a fun approach.

	 */

	public static void main(String[] args) {
		SpringApplication.run(FootballapiApplication.class, args);

	}

}
