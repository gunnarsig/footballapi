package no.noroff.footballapi;

public class FootballInformation {

    private double Classic1X2Probability;
    private boolean isExpired;

    public double getClassic1X2Probability() {
        return Classic1X2Probability;
    }

    public void setClassic1X2Probability(double classic1x2Probability) {
        Classic1X2Probability = classic1x2Probability;
    }

    public boolean getIsExpired() {return isExpired;}

    public void setIsExpired(boolean bool) {
        isExpired = bool;
    }


}
