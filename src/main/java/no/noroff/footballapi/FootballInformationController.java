package no.noroff.footballapi;

import com.mashape.unirest.http.*;
import org.json.*;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.format.DateTimeFormatter;
import java.time.*;
import java.util.*;


@RestController
public class FootballInformationController {

    private static ZonedDateTime getApiTimestamp() {
        return ZonedDateTime.now(ZoneId.of("Europe/London"));
    }

    private String authKey = "17654cd0d9e23b8e2fd18bd42e90b557";
    String tomorrow = getApiTimestamp().plusDays(1).format(DateTimeFormatter.ISO_LOCAL_DATE);
//    private static String predictionEndpoint = "https://football-prediction-api.p.rapidapi.com/api/v2/predictions";
private static String predictionEndpoint = "https://api.the-odds-api.com/v3/odds?sport=soccer_epl&region=uk";


    @RequestMapping("/")
    public FootballInformation getClassic() throws  Exception{
        HttpResponse response = Unirest
                .get(predictionEndpoint)
                .queryString("apiKey", authKey)
                .asJson();


        JSONObject jo = new JSONObject(response.getBody());

//        System.out.println(jo.toString());

        JSONArray jsonArray = jo.getJSONArray("array");
        JSONArray data = jsonArray.getJSONObject(0).getJSONArray("data");

        for (int i = 0; i < data.length(); i++) {
//            System.out.println(jsonArray.toString());
            System.out.println(data.get(0));
        }

        return new FootballInformation();




    }

}
